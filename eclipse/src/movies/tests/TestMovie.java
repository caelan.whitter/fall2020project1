package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;

class TestMovie {
	/**
	 * @author Momshad Hussain
	 */ 
	@Test
	public void testGetters() {
		Movie movies = new Movie("2019","Avengers End Game","182","la");
		String result = movies.getReleaseYear();
		String result2 = movies.getNameOfMOvie();
		String result3 = movies.getRunTime();
		String result4 = movies.getSource();
		assertEquals("2019", result);
		assertEquals("Avengers End Game", result2);
		assertEquals("182", result3);
		assertEquals("la", result4);
	}
	/**
	 * @author Caelan Whitter
	 */
	@Test
	public void testSetters() {
		Movie movies2 = new Movie("1994","Pulp Fiction","154","kaggle");
		movies2.SetMovieReleaseYear("1995");
		movies2.SetMovieName("pulp fiction");
		movies2.SetMovieRuntime("154 minutes");
		movies2.SetMoviesource("kaggle");
		String result = movies2.getReleaseYear();
		String result2 = movies2.getNameOfMOvie();
		String result3 = movies2.getRunTime();
		String result4 = movies2.getSource();
		assertEquals("1995",result);
		assertEquals("pulp fiction",result2);
		assertEquals("154 minutes",result3);
		assertEquals("kaggle",result4);
		
	}

}
