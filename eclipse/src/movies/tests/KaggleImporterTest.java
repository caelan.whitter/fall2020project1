package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleImporter;

class KaggleImporterTest {
	/**
	 * @author Caelan Whitter
	 */
	
	@Test
	public void testKaggleImporter() {
		ArrayList<String> testing = new ArrayList<String>();
		testing.add("Johnny Depp	Gwyneth Paltrow	Ewan McGregor	Olivia Munn	Jeff Goldblum	Paul Bettany	\"Juggling some angry Russians, the British Mi5, his impossibly leggy wife and an international terrorist, debonair art dealer and part time rogue Charlie Mortdecai (Johnny Depp) must traverse the globe armed only with his good looks and special charm in a race to recover a stolen painting rumored to contain the code to a lost bank account filled with Nazi gold. (c) Lionsgate\"	David Koepp	Director Not Available	Director Not Available	Action	R 	1/23/2015	106 minutes	Liongate Films	Mortdecai	Eric Aronson	Writer Not Available	Writer Not Available	Writer Not Available	2015");
		testing.add("Brett Granstaff	Diahann Carroll	Lara Jean Chorostecki	Roddy Piper	T.J. McGibbon	James Preston Rogers	\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\"	Warren P. Sonoda	Director Not Available	Director Not Available	Action	PG-13 	1/8/2016	111 minutes	Freestyle Releasing	The Masked Saint	Scott Crowell	Brett Granstaff	Writer Not Available	Writer Not Available	2016");

		KaggleImporter kaggle = new KaggleImporter(null,null);
		ArrayList<String> results = kaggle.process(testing);
		assertEquals(results.get(0),"2015	Mortdecai	106 minutes	kaggle");
		assertEquals(results.get(1),"2016	The Masked Saint	111 minutes	kaggle");
		
	
	}

}
