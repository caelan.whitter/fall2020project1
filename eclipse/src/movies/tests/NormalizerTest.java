package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Normalizer;

class NormalizerTest {
	/**
	 * @author Caelan Whitter
	 */
	@Test
	public void testNormalizer()
	{
		ArrayList<String> testing = new ArrayList<String>();
		testing.add("2015	Mortdecai	106 minutes	kaggle");
		testing.add("2016	The Masked Saint	111 minutes	kaggle");
		Normalizer normalizing = new Normalizer(null,null);
		ArrayList<String> result = normalizing.process(testing);
		assertEquals(result.get(0),"2015	mortdecai	106	kaggle");
		assertEquals(result.get(1),"2016	the masked saint	111	kaggle");
		
		
		
	}
}
