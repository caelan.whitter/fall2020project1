package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

/**
 * @author Momshad Hussain
 */ 
import org.junit.jupiter.api.Test;

import movies.importer.ImdbImporter;


/**
 * @author Momshad Hussain
 */ 
class imdbImporterTest {

	/**
	 * @author Momshad Hussain
	 */ 
	@Test
	public void imbdProcessTest() {
		ArrayList<String> testData = new ArrayList<String>();
			testData.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2 ");
			testData.add("tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"				7	7");
	        ImdbImporter imp = new ImdbImporter(null,null);
	        ArrayList<String> results =  imp.process(testData);
	        assertEquals(results.get(0),"1894	Miss Jerry	45	imdb"); 
	        assertEquals(results.get(1),"1906	The Story of the Kelly Gang	70	imdb");     
	}

}
