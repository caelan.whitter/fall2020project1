	package movies.importer;
	
	import java.util.ArrayList;
	
	/**
	 * @author Momshad Hussain
	 */ 
	public class Validator extends Processor { 
		
		/**
		 * @author Momshad Hussain
		 */ 
		public Validator(String sourceDir, String outputDir) {
			super(sourceDir, outputDir, false);
		}
	
		/**
		 * @author Momshad Hussain
		 */ 
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> validated = new ArrayList<String>();
		for (int i = 0; i<input.size(); i++)
		{
			String[] currentMovieDetails= input.get(i).split("\t");
			String currentMovieTitle = currentMovieDetails[1];
			String currentMovieReleaseYear = currentMovieDetails[0];
			String currentMovieRunTime = currentMovieDetails[2];
			if (currentMovieTitle != null || currentMovieReleaseYear != null || currentMovieRunTime != null ) {
				validated.add(input.get(i));

			
		try {
			int formattedReleaseYear = Integer.parseInt(currentMovieReleaseYear);
			int formattedRuntTime = Integer.parseInt(currentMovieRunTime);
			
			}
		
			catch(NumberFormatException ex) {
					
					continue;
				}
			}
			}
		return validated;
	}
}
