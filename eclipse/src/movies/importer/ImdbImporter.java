package movies.importer;

import java.util.ArrayList;

/**
 * @author Momshad Hussain
 */ 
public class ImdbImporter extends Processor {

	/**
	 * @author Momshad Hussain
	 */ 
	public ImdbImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}


	/**
	 * @author Momshad Hussain
	 */ 
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> standardized = new ArrayList<String>();
		for (int i = 0; i<input.size(); i++)
		{
			String[] currentMovieDetails= input.get(i).split("\t", -1);
			String currentMovieStanDetails = 
					currentMovieDetails[3] + "\t" 
					+currentMovieDetails[1] + "\t" 
					+currentMovieDetails[6] + "\t" 
					+"imdb";		
			standardized.add(currentMovieStanDetails);			
		}
		
		return standardized;
	}

}
