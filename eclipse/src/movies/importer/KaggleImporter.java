package movies.importer;

import java.util.List;
import java.util.ArrayList;
/**
 * 
 * @author Caelan
 *
 */
public class KaggleImporter extends Processor {
	
	public KaggleImporter(String source,String destination)
	{
		super(source,destination,true);
	}
	
	public ArrayList<String> process (ArrayList<String> words)
	{
		ArrayList<String> standardized = new ArrayList<String>();
		
		
		for (String line: words)
		{
			String[] fields=line.split("\\t");
			if (fields.length==21)
			{
				String releaseYear = fields[20];
				String name = fields[15];
				String runtime = fields[13];
				String source= "kaggle";
				Movie movie= new Movie(releaseYear,name,runtime,source);
				standardized.add(movie.toString());
			}
		}
		
		return standardized;
	}

}
