package movies.importer;

import java.util.ArrayList;

public class Deduper extends Processor {
	/**
	 * @author Caelan
	 */

	public Deduper(String sourceDir, String outputDir)
	{
		super(sourceDir, outputDir, false);
	}
	
	/**
	 * @author Caelan
	 * @author Momshad
	 */
	public ArrayList<String> process (ArrayList<String> words)
	{
		ArrayList<String> deduped = new ArrayList<String>();
		
		ArrayList<Movie> movies = new ArrayList<Movie>();
		
		for (String line: words)
		{
			String[] fields=line.split("\\t");

				String releaseYear = fields[0];
				String name = fields[1];
				String runtime = fields[2];
				String source= fields[3];
				Movie movie= new Movie(releaseYear,name,runtime,source);
			    
				if(movies.contains(movie))
				{
					int indexOf = movies.indexOf(movie);
					if (!movies.get(indexOf).getSource().equals(source))
					{
						String mergedSource = movies.get(indexOf).getSource() +";"+source;
						movies.get(indexOf).SetMoviesource(mergedSource);
					}
				}
				else
				{
					movies.add(movie);
				}
				
		}
		
		for (Movie movie: movies)
		{
			deduped.add(movie.toString());
		}
		
		return deduped;
	}

	
	

}
