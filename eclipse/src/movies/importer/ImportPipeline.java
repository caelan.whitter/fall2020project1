package movies.importer;

import java.io.IOException;

/**
 * @author Caelan
 * @author Momshad
 */
public class ImportPipeline {

	public static void main(String[] args) throws IOException {
		//Importing
		//kaggle
		String srcDir1 = "C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testInputPro";
		//imdb
		String srcDir2 = "C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testInputPro2";
		//importer output
		String desDir1 = "C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testOutputPro1";
		
		//kaggle
		KaggleImporter imp = new KaggleImporter(srcDir1,desDir1);
		//imdb
		ImdbImporter imp2 = new ImdbImporter(srcDir2,desDir1);
		
		//Validating
		String srcDir3 = "C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testOutputPro1";
		String desDir2 = "C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testOutputPro3";
		Validator validate = new Validator(srcDir3,desDir2);
		//Normalizing
		String srcDir4="C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testOutputPro3";
		String desDir3="C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testOutputPro4";
		Normalizer normalize = new Normalizer(srcDir4,desDir3);
		
		
		//Deduper
		String srcDir5="C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testOutputPro4";
		String desDir4="C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testOutputProFinal";
		Deduper deduped = new Deduper(srcDir5,desDir4);
		
		Processor [] pro = {imp,imp2,validate,normalize,deduped};
		processAll(pro);
		
		
	}
	
	public static void processAll(Processor[] pro)  throws IOException 
	{
		for (int i = 0;i<pro.length;i++)
		{
			pro[i].execute();
		}
	}
 
}
