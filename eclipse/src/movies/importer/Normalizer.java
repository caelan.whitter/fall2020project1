package movies.importer;

import java.util.ArrayList;
/**
 * 
 * @author Caelan
 *
 */
	public class Normalizer extends Processor {

	public Normalizer(String sourceDir, String outputDir) 
	{
		super(sourceDir, outputDir, false);

	}
	
	public ArrayList<String> process (ArrayList<String> words)
	{
		ArrayList<String> normalized = new ArrayList<String>();
		
		for (String line: words)
		{
			String[] fields=line.split("\\t");
			
			String releaseYear = fields[0];
			String name = fields[1].toLowerCase();
			
			String [] runT = fields[2].split(" ");
			String runtime = runT[0];
			
			String source= fields[3];
			
			Movie movie= new Movie(releaseYear,name,runtime,source);
			normalized.add(movie.toString());

		}

		return normalized;
	}

}
