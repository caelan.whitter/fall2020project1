package movies.importer;

public class Movie {
	private String releaseYear;
	private String name;
	private String runtime;
	private String source;
	
	/**
	 * @author Caelan
	 */
	public Movie(String releaseYear,String name,String runtime,String source)
	{
		this.releaseYear=releaseYear;
		this.name=name;
		this.runtime=runtime;
		this.source=source;
	}
	/**
	 * @author Caelan
	 */
	public void SetMovieReleaseYear(String newreleaseYear) {
		this.releaseYear = newreleaseYear;
	}
	
	/**
	 * @author Caelan
	 */
	public void SetMovieName(String newName) {
		this.name = newName;
	}
	/**
	 * @author Caelan
	 */
	public void SetMovieRuntime(String newRuntime) {
		this.runtime = newRuntime;
	}
	/**
	 * @author Caelan
	 */
	public void SetMoviesource(String newSource) {
		this.source = newSource;
	}
	/**
	 * @author Momshad Hussain
	 */   
	public String getReleaseYear()
	{
		return this.releaseYear;
	}	
	
	/**
	 * @author Momshad Hussain
	 */  
	public String getNameOfMOvie()
	{
		return this.name;
	}
	
	/**
	 * @author Momshad Hussain
	 */  
	public String getRunTime()
	{
		return this.runtime;
	}
	
	/**
	 * @author Momshad Hussain
	 */  
	public String getSource()
	{
		return this.source;
	}
	
	/**
	 * @author Momshad Hussain
	 */  
	public String toString() {
		return 
				this.releaseYear+ 
				"\t" +this.name+ "\t"  
				+this.runtime+ "\t"  
				+this.source;	
	}
	/**
	 * @author Caelan
	 * @author Momshad
	 */
	@Override
	public boolean equals( Object other)
	{
		if (this==other)
		{
			return true;
		}
		if(other==null)
		{
			return false;
		}
		if(this.getClass()!=other.getClass())
		{
			return false;
		}
		Movie movie = (Movie)other;
		if(!this.name.equals(movie.name))
		{
			return false;
		}
		if(!this.releaseYear.contentEquals(movie.releaseYear))
		{
			return false;
		}
		int difference = Integer.parseInt(this.runtime) - Integer.parseInt(movie.runtime);
		if(difference<-5 || difference>5)
		{
			return false;
		}
		return true;
	}
	
}
